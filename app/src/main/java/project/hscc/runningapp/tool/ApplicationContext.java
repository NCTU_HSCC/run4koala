package project.hscc.runningapp.tool;

import android.Manifest;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

public class ApplicationContext extends Application {
    private static final String TAG = ApplicationContext.class.getSimpleName();

    public static final String APPLICATION_PREFERENCES = "APPLICATION_PREFERENCES";
    public static final String USERNAME_PREFERENCES = "USERNAME_PREFERENCES";
    public static final String KOALA_MAC_PREFERENCES = "KOALA_MAC_PREFERENCES";

    private static ApplicationContext mInstance;

    public static final int REQUEST_COARSE_LOCATION = 0x01 << 1;
    public static final int REQUEST_EXTERNAL_STORAGE = 0x01 << 2;
    public static final int REQUEST_FINE_LOCATION = 0x01 << 3;

    public static final String REQUEST_URL = "http://52.68.81.217:8081/coaching/v1/nctu/rawdata";
    public static final String REQUEST_OURURL = "http://sports1.1210.tw/api/record";
    public static final String REGISTE_URL = "http://sports1.1210.tw/api/user";


    public static String[] PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    public static String user_name, mac;
    public static double bodyMassKg;

    public static long record_time;
    public static double gps_latitude, gps_longitude, gps_height, gps_speed, gps_avrgSpeed;
    public static double acceleration_x, acceleration_y, acceleration_z;
    public static double angular_velocity_x,angular_velocity_y,angular_velocity_z;
    public static double step_count, step_frequency, step_amplitude, landing_time, vertical_amplitude, landing_impact;
    public static String running_type="heel";
    public static double pace; //should be pace
    public static float distance;
    public static double realSpeed;
    public static double avgSpeed;
    public static int state_run = 1; //1: running, 2: biking;
    public static int state_break = 2;


    public static ApplicationContext getInstance(){
        ApplicationContext mApplication = mInstance;
        if(mInstance == null){
            mInstance = new ApplicationContext();
        }
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        init();
    }

    private void init() {
        SharedPreferences mPref = getSharedPreferences(APPLICATION_PREFERENCES, Context.MODE_PRIVATE);
        user_name =  mPref.getString(USERNAME_PREFERENCES, "User");
        mac = mPref.getString(KOALA_MAC_PREFERENCES, "unknown");
    }

    public void savePreferences() {
        SharedPreferences sharedPreferences = getSharedPreferences(APPLICATION_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USERNAME_PREFERENCES, user_name);
        editor.putString(KOALA_MAC_PREFERENCES, mac);
        editor.apply();
    }

}
