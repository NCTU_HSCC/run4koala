package project.hscc.runningapp.runtilldie;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import project.hscc.runningapp.R;

public class Main2Activity extends AppCompatActivity {

    Calendar c;
    SimpleDateFormat sdf;
    TextView tv;
    String[] koala = {"Runing", "Walking", "Calories"};
    int index_yogurt = 0;
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        c = Calendar.getInstance();
        sdf = new SimpleDateFormat("yyyy-MM-dd");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        tv = (TextView) findViewById(R.id.textView5);
        tv.setText(sdf.format(c.getTime()));

        ImageView imageView = (ImageView) findViewById(R.id.imageView9);
        imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                c.add(Calendar.DATE, -1);
                tv.setText(sdf.format(c.getTime()));// Do something in response to button click
            }
        });

        ImageView imageView1 = (ImageView) findViewById(R.id.imageView10);
        imageView1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                c.add(Calendar.DATE, 1);
                tv.setText(sdf.format(c.getTime()));// Do something in response to button click
            }
        });

        tv1 = (TextView)findViewById(R.id.textView6);
        tv1.setText(koala[index_yogurt]);

        ImageView imageView2 = (ImageView) findViewById(R.id.imageView12);
        imageView2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                tv1.setText(koala[index_yogurt]);
                index_yogurt = (index_yogurt +1) % 3;
            }
        });

        tv1 = (TextView)findViewById(R.id.textView6);
        tv1.setText(koala[index_yogurt]);

        ImageView imageView3 = (ImageView) findViewById(R.id.imageView11);
        imageView3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                tv1.setText(koala[index_yogurt]);
                index_yogurt = (index_yogurt -1) % 3;
            }
        });


    }


}

