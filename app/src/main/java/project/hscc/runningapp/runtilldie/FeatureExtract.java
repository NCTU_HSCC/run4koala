package project.hscc.runningapp.runtilldie;
import org.apache.commons.math3.analysis.function.Abs;
import org.apache.commons.math3.analysis.function.Pow;
import org.apache.commons.math3.stat.descriptive.moment.Kurtosis;
import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.Skewness;
import org.apache.commons.math3.stat.descriptive.moment.Variance;

import java.util.ArrayList;


public class FeatureExtract {


    public static ArrayList<Double> main(ArrayList<Double> C1,ArrayList<Double> C2,ArrayList<Double> C3) {

        //For testing feature extraction
     /* C1.add(1.5);
        C1.add(2.5);
        C1.add(3.5);
        C1.add(1.5);

        C2.add(1.5);
        C2.add(2.5);
        C2.add(1.5);
        C2.add(3.5);

        C3.add(3.5);
        C3.add(1.5);
        C3.add(1.5);
        C3.add(2.5); */



        Element e1 = new Element(C1);
        Element e2 = new Element(C2);
        Element e3 = new Element(C3);




        ArrayList<Double> features = new ArrayList<Double>();

        features.add(e1.getMean());
        features.add(e2.getMean());
        features.add(e3.getMean());
        features.add(e1.getVar());
        features.add(e2.getVar());
        features.add(e3.getVar());
        features.add(e1.getSkew());
        features.add(e2.getSkew());
        features.add(e3.getSkew());
        features.add(e1.getKurt());
        features.add(e2.getKurt());
        features.add(e3.getKurt());
        features.add(Double.valueOf(e1.getZCR()));
        features.add(Double.valueOf(e2.getZCR()));
        features.add(Double.valueOf(e3.getZCR()));
        features.add(Double.valueOf(e1.getMCR()));
        features.add(Double.valueOf(e2.getMCR()));
        features.add(Double.valueOf(e3.getMCR()));
        features.add(e1.getAAD());
        features.add(e2.getAAD());
        features.add(e3.getAAD());
        features.add(e1.getAAV());
        features.add(e2.getAAV());
        features.add(e3.getAAV());
        features.add(e1.getRMS());
        features.add(e2.getRMS());
        features.add(e3.getRMS());
        features.add(getCov(e1, e2));
        features.add(getCov(e2, e3));
        features.add(getCov(e1, e3));
        features.add(getARA(e1, e2, e3));

        //System.out.println(features.toString());

        return features;


    }


    private static class Element {

        public ArrayList<Double> dataset = null;

        public Element(ArrayList<Double> dataset) {

            this.dataset = dataset;
        }

        public double getMean() {
            Mean mean = new Mean();
            for (int i = 0; i < dataset.size(); i++) {
                mean.increment(dataset.get(i));
            }
            return mean.getResult();
        }

        public double getVar() {
            Variance var = new Variance();
            for (int i = 0; i < dataset.size(); i++) {
                var.increment(dataset.get(i));
            }
            return var.getResult();
        }

        public double getSkew() {
            Skewness skew = new Skewness();
            for (int i = 0; i < dataset.size(); i++) {
                skew.increment(dataset.get(i));
            }
            return skew.getResult();
        }

        public double getKurt() {
            Kurtosis kurt = new Kurtosis();
            for (int i = 0; i < dataset.size(); i++) {
                kurt.increment(dataset.get(i));
            }
            return kurt.getResult();
        }

        public int getZCR() {
            int count = 0;
            for (int i = 0; i < dataset.size() - 1; i++) {
                if (dataset.get(i) * dataset.get(i + 1) < 0) {
                    count++;
                }
            }
            return count;
        }

        public int getMCR() {
            double mean = getMean();
            int count = 0;
            for (int i = 0; i < dataset.size() - 1; i++) {
                if ((dataset.get(i) > mean && dataset.get(i + 1) < mean)
                        || (dataset.get(i) < mean && dataset.get(i + 1) > mean)) {
                    count++;
                }
            }
            return count;
        }

        public double getAAD() {
            double dif = 0;
            Abs abs = new Abs();
            for (int i = 0; i < dataset.size() - 1; i++) {
                dif += abs.value(dataset.get(i + 1) - dataset.get(i));
            }
            return dif;
        }

        public double getAAV() {
            double sum = 0;
            Abs abs = new Abs();
            for (int i = 0; i < dataset.size(); i++) {
                sum += abs.value(dataset.get(i));
            }
            return sum / dataset.size();
        }

        public double getRMS() {
            double sum = 0;
            Pow pow = new Pow();
            for (int i = 0; i < dataset.size(); i++) {
                sum += pow.value(dataset.get(i), 2);
            }
            return pow.value(sum / dataset.size(), 0.5);
        }

        public ArrayList<Double> fft_magnitude(boolean DIRECT) {
            // - n is the dimension of the problem
            // - nu is its logarithm in base e
            int n = dataset.size();
            double[] inputImag = new double[n];

            // If n is a power of 2, then ld is an integer (_without_ decimals)
            double ld = Math.log(n) / Math.log(2.0);

            // Here I check if n is a power of 2. If exist decimals in ld, I
            // quit
            // from the function returning null.
            if (((int) ld) - ld != 0) {
                System.out.println("The number of elements is not a power of 2.");
                return null;
            }

            // Declaration and initialization of the variables
            // ld should be an integer, actually, so I don't lose any
            // information in
            // the cast
            int nu = (int) ld;
            int n2 = n / 2;
            int nu1 = nu - 1;
            double[] xReal = new double[n];
            double[] xImag = new double[n];
            double tReal, tImag, p, arg, c, s;

            // Here I check if I'm going to do the direct transform or the
            // inverse
            // transform.
            double constant;
            if (DIRECT)
                constant = -2 * Math.PI;
            else
                constant = 2 * Math.PI;

            // I don't want to overwrite the input arrays, so here I copy them.
            // This
            // choice adds \Theta(2n) to the complexity.
            for (int i = 0; i < n; i++) {
                xReal[i] = dataset.get(i);
                xImag[i] = inputImag[i];
            }

            // First phase - calculation
            int k = 0;
            for (int l = 1; l <= nu; l++) {
                while (k < n) {
                    for (int i = 1; i <= n2; i++) {
                        p = bitreverseReference(k >> nu1, nu);
                        // direct FFT or inverse FFT
                        arg = constant * p / n;
                        c = Math.cos(arg);
                        s = Math.sin(arg);
                        tReal = xReal[k + n2] * c + xImag[k + n2] * s;
                        tImag = xImag[k + n2] * c - xReal[k + n2] * s;
                        xReal[k + n2] = xReal[k] - tReal;
                        xImag[k + n2] = xImag[k] - tImag;
                        xReal[k] += tReal;
                        xImag[k] += tImag;
                        k++;
                    }
                    k += n2;
                }
                k = 0;
                nu1--;
                n2 /= 2;
            }

            // Second phase - recombination
            k = 0;
            int r;
            while (k < n) {
                r = bitreverseReference(k, nu);
                if (r > k) {
                    tReal = xReal[k];
                    tImag = xImag[k];
                    xReal[k] = xReal[r];
                    xImag[k] = xImag[r];
                    xReal[r] = tReal;
                    xImag[r] = tImag;
                }
                k++;
            }

            // Here I have to mix xReal and xImag to have an array (yes, it
            // should
            // be possible to do this stuff in the earlier parts of the code,
            // but
            // it's here to readibility).
            ArrayList<Double> newArray = new ArrayList<Double>(xReal.length);
            //float[] newArray = new float[xReal.length];
            double radice = 1 / Math.sqrt(n);
            for (int i = 0; i < xReal.length; i++) {

                // I Modified Original Program so that it will only produce fft
                // magnitude array.
                newArray.add((double) Math.pow((Math.pow(xReal[i] * radice, 2) + Math.pow(xImag[i] * radice, 2)), 0.5));
            }
            return newArray;
        }

        private static int bitreverseReference(int j, int nu) {
            int j2;
            int j1 = j;
            int k = 0;
            for (int i = 1; i <= nu; i++) {
                j2 = j1 / 2;
                k = 2 * k + j1 - 2 * j2;
                j1 = j2;
            }
            return k;
        }
    }

    private static double getCov(Element e1, Element e2) {
        double sum = 0;
        for (int i = 0; i < e1.dataset.size(); i++) {
            sum += (e1.dataset.get(i) - e1.getMean()) * (e2.dataset.get(i) - e2.getMean());
        }
        return sum / (e1.dataset.size() - 1);
    }

    private static double getARA(Element e1, Element e2, Element e3) {
        double sum = 0, tmp = 0;
        Pow pow = new Pow();
        for (int i = 0; i < e1.dataset.size(); i++) {
            tmp = pow.value(e1.dataset.get(i), 2) + pow.value(e2.dataset.get(i), 2) + pow.value(e3.dataset.get(i), 2);
            sum += pow.value(tmp, 0.5);
        }
        return sum / e1.dataset.size();
    }

    private static double getMag(Element e1, Element e2, Element e3, int idx) {
        double sum = 0;
        Pow pow = new Pow();
        if (idx > e1.dataset.size()) {
            return -1;
        }
        idx -= 1;
        sum = pow.value(e1.dataset.get(idx), 2) + pow.value(e2.dataset.get(idx), 2) + pow.value(e3.dataset.get(idx), 2);
        return pow.value(sum, 0.5);
    }

}
