package project.hscc.runningapp.runtilldie;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import org.json.JSONObject;

import project.hscc.runningapp.R;


public class SettingsActivity extends AppCompatActivity {
    private static final int REQUEST_PICK_IMG = 0;
    private ImageView upic;
    private TextInputEditText input_firstname;
    private TextInputEditText input_lastname;
    private TextInputEditText input_age;
    private TextInputEditText input_height;
    private TextInputEditText input_weight;
    private RadioGroup radioSexGroup;
    private Button btn_save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        upic = (ImageView) findViewById(R.id.pic);
        input_firstname = (TextInputEditText) findViewById(R.id.input_firstname);
        input_lastname = (TextInputEditText) findViewById(R.id.input_lastname);
        input_age = (TextInputEditText) findViewById(R.id.input_age);
        input_height = (TextInputEditText) findViewById(R.id.input_height);
        input_weight = (TextInputEditText) findViewById(R.id.input_weight);
        radioSexGroup =(RadioGroup) findViewById(R.id.radioSex);
        btn_save = (Button) findViewById(R.id.btn_save);

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveProfile();
            }
        });

        upic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage(v);
            }
        });
    }

    public void saveProfile() {
        JSONObject uprofile = new JSONObject();
        try {
            uprofile.put("First Name", input_firstname.getText().toString());
            uprofile.put("Last Name", input_lastname.getText().toString());
            uprofile.put("Age", input_age.getText().toString());
            uprofile.put("Height", input_height.getText().toString());
            uprofile.put("Weight", input_weight.getText().toString());

            int selectedId = radioSexGroup.getCheckedRadioButtonId();
            RadioButton radioSexButton = (RadioButton) findViewById(selectedId);
            uprofile.put("Gender", radioSexButton.getText().toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
        Toast.makeText(this, uprofile.toString(), Toast.LENGTH_LONG).show();
    }

    public void chooseImage(View view) {
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        // Start the Intent
        startActivityForResult(Intent.createChooser(intent, "Select a picture"), REQUEST_PICK_IMG);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // When an Image is picked
        if (requestCode == REQUEST_PICK_IMG && resultCode == RESULT_OK && data != null) {

            // Get the Image from data
            Uri selectedImage = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                upic.setImageBitmap(bitmap);
            } catch (Exception e) {
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, "You haven't picked Image", Toast.LENGTH_LONG).show();
        }
    }

}

