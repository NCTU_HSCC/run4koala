package project.hscc.runningapp.runtilldie;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import project.hscc.runningapp.R;


public class SignupActivity extends AppCompatActivity {

    private TextInputEditText input_email;
    private TextInputEditText input_passwd;
    private TextInputEditText input_repasswd;
    private Button btn_signup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        input_email = (TextInputEditText) findViewById(R.id.input_email);
        input_passwd = (TextInputEditText) findViewById(R.id.input_passwd);
        input_repasswd = (TextInputEditText) findViewById(R.id.input_repasswd);
        btn_signup = (Button) findViewById(R.id.btn_signup);

        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });
    }

    public void signup() {

        if (!validate()) {
            onSignupFailed();
            return;
        }

        btn_signup.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(SignupActivity.this,
                R.style.AppTheme_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Creating Account...");
        progressDialog.show();

        final String email = input_email.getText().toString();
        final String password = input_passwd.getText().toString();
        final String reEnterPassword = input_repasswd.getText().toString();

        // TODO: Implement your own signup logic here.

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onSignupSuccess or onSignupFailed
                        // depending on success
                        onSignupSuccess(email);
                        // onSignupFailed();
                        progressDialog.dismiss();
                    }
                }, 3000);
    }

    public boolean validate() {
        boolean valid = true;

        String email = input_email.getText().toString();
        String password = input_passwd.getText().toString();
        String reEnterPassword = input_repasswd.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            input_email.setError("please enter a valid email address");
            valid = false;
        } else {
            input_email.setError(null);
        }

        if (password.isEmpty()) {
            input_passwd.setError("please input a password");
            valid = false;
        } else {
            input_passwd.setError(null);
        }

        if (reEnterPassword.isEmpty() || !(reEnterPassword.equals(password))) {
            input_repasswd.setError("please check your password");
            valid = false;
        } else {
            input_repasswd.setError(null);
        }

        return valid;
    }

    public void onSignupSuccess(String email) {
        btn_signup.setEnabled(true);

        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putString("umail", email);
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        finish();
    }

    public void onSignupFailed() {
        Toast.makeText(getBaseContext(), "Sign up failed", Toast.LENGTH_LONG).show();

        btn_signup.setEnabled(true);
    }
}
