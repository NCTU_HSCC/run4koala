package project.hscc.runningapp.runtilldie;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import project.hscc.runningapp.R;

public class subHistoryMainActivity extends AppCompatActivity {
    ListView listView;
    String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"};
    int permsRequestCode = 200;
    ArrayAdapter<String> listAdapter;
    ArrayList<String> data = new ArrayList<String>();
    String test = "";
    ArrayList<String> list = new ArrayList<String>();
    String id="";


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subhistory_main);
        requestPermissions(perms, permsRequestCode);
        Intent intent=this.getIntent();
        id=intent.getStringExtra("id");
        update();
        listView = (ListView)findViewById(R.id.lv);
        listAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,data);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getApplicationContext(), "你選擇的是" + list.get(position), Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void update(){
        final File mSDFile = new File(Environment.getExternalStorageDirectory().getPath());
        String a = Environment.getExternalStorageDirectory().getAbsolutePath().toString();
        try{
            FileReader fr = new FileReader(mSDFile.getAbsolutePath()+"/Data/history.csv");
            BufferedReader br = new BufferedReader(fr);
            String temp = br.readLine();
            String [] attr=temp.split(",");
            while (temp!=null){
                temp=br.readLine();
                String [] str=temp.split(",");
                Log.i("info",str.toString());
                if(id.equals(str[0])){
                    for(int j = 4; j <14; j++){
                        data.add(attr[j]+" :   "+str[j]);
                        //Toast.makeText(getApplicationContext(), "你選擇的是" + data.get(j), Toast.LENGTH_SHORT).show();
                    }
                    //test = data.get(0)+", "+data.get(1)+"\n"+data.get(2)+", Duration: "+data.get(3);
                    //list.add(test);
                    for(int j =10 ; j >= 0; j--)
                        data.remove(j);
                }

            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults){
        switch (permsRequestCode) {

            case 200:

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
               /*get permission success*/

                } else {
               /*get permission fail*/
                }
                break;

            default:
                break;
        }
    }
}
