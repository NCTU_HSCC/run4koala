package project.hscc.runningapp.runtilldie;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.security.MessageDigest;

import project.hscc.runningapp.R;

public class LoginActivity extends AppCompatActivity {

    private static final int REQUEST_SIGNUP = 0;

    private TextInputEditText input_email;
    private TextInputEditText input_passwd;
    private Button btn_login;
    private TextView link_signup;
    private TextView link_forgetpasswd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        input_email = (TextInputEditText) findViewById(R.id.input_email);
        input_passwd = (TextInputEditText) findViewById(R.id.input_password);
        btn_login = (Button) findViewById(R.id.btn_login);
        link_signup = (TextView) findViewById(R.id.link_signup);
        link_forgetpasswd = (TextView) findViewById(R.id.link_forgetpasswd);

        btn_login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
            }
        });

        link_signup.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Start the Signup activity
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
                //finish();
                //overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });

        link_forgetpasswd.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(intent);
            }
        });

    }

    public void login() {

        if (!validate()) {
            onLoginFailed();
            return;
        }

        btn_login.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this, R.style.AppTheme_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();

        final String email = input_email.getText().toString();
        final String passwd = input_passwd.getText().toString();

        // TODO: encrypt passwd
        // Toast.makeText(getBaseContext(), md5(passwd), Toast.LENGTH_LONG).show();

        // TODO: Implement your own authentication logic here.
        boolean authResult = false;
        if(email.equals("test@tmail.com") && passwd.equals("1234")) {
            authResult = true;
        }

        final boolean isAuth;
        isAuth = authResult;

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed
                        if(isAuth) {
                            onLoginSuccess(email);
                        } else {
                            onLoginFailed();
                        }
                        progressDialog.dismiss();
                    }
                }, 3000);
    }

    public boolean validate() {
        boolean valid = true;

        String email = input_email.getText().toString();
        String password = input_passwd.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            input_email.setError("please enter a valid email address");
            valid = false;
        } else {
            input_email.setError(null);
        }

        if (password.isEmpty()) {
            input_passwd.setError("please enter a password");
            valid = false;
        } else {
            input_passwd.setError(null);
        }

        return valid;
    }

    public void onLoginSuccess(String email) {
        btn_login.setEnabled(true);

        // remember login status
        getSharedPreferences("login_status", MODE_PRIVATE).edit()
                .putString("email", input_email.getText().toString())
                .putString("passwd", input_passwd.getText().toString())
                .apply();

        Intent intent = new Intent(this, RunningActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("umail", email);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        btn_login.setEnabled(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {
                // TODO: Implement successful signup logic here
                // By default we just finish the Activity and log them in automatically
                Intent intent = new Intent(this, RunningActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("umail", data.getExtras().getString("umail"));
                intent.putExtras(bundle);
                startActivity(intent);
                this.finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        // disable going back to the MainActivity
        moveTaskToBack(true);
    }

    private static final String md5(final String password) {

        try {

            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(password.getBytes());
            byte messageDigest[] = digest.digest();

            StringBuilder hexString = new StringBuilder();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

}
