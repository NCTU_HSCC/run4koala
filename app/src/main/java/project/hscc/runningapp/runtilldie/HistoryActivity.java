package project.hscc.runningapp.runtilldie;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

import project.hscc.runningapp.R;

public class HistoryActivity extends AppCompatActivity {
    ListView listView;
    String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"};
    int permsRequestCode = 200;
    private ArrayList<View> arrayList=new ArrayList<View>();
    String attr;
    MyAdpater listAdapter=new MyAdpater();
    ArrayList<String> data = new ArrayList<String>();
    String test = "";
    ArrayList<String> id = new ArrayList<String>();
    ArrayList<String> list = new ArrayList<String>();
    ArrayList<String> deleteid = new ArrayList<String>();
    ArrayList<String> totaldata = new ArrayList<String>();
    ArrayList<String> attrdata = new ArrayList<String>();
    int list_length = 0;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_main);
        requestPermissions(perms, permsRequestCode);
        update();
        listView = (ListView) findViewById(R.id.lv);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long a) {
                //Toast.makeText(getApplicationContext(), "你選擇的是" + position, Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(HistoryActivity.this,subHistoryMainActivity.class);
                intent.putExtra("id",id.get(position));
                startActivity(intent);
            }
        });

    }
    public class Holder{
        TextView textview;
        Button delete;
    }
    Holder holder;

        private class MyAdpater extends BaseAdapter{
            @Override
            public int getCount() {
                return list.size();
            }
            @Override
            public Object getItem(int position) {
                return null;
            }
            @Override
            public long getItemId(int position) {
                return 0;
            }
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = convertView;

                final int index=position;
                list_length = list.size();
                    if(v == null){
                        v = LayoutInflater.from(getApplicationContext()).inflate(R.layout.listdesign, null);
                        holder = new Holder();
                        holder.delete = (Button) v.findViewById(R.id.deletebutton);
                        holder.textview = (TextView) v.findViewById(R.id.textview);
                        v.setTag(holder);

                    }else{
                        holder = (Holder) v.getTag();

                    }
                if (list.get(position)!=null){
                   // System.out.println("-------------------------------------------------------");
                   // System.out.println(deleteid.get(position));
                        //System.out.println("i");

                        holder.textview.setText(list.get(position));

                        holder.delete.setOnClickListener(new deleteclick(position));
                }
//                arrayList.add(v);
//                this.notifyDataSetChanged();
                return v;
            }
            class deleteclick implements View.OnClickListener {
                private int position;
                deleteclick(int pos){
                    position=pos;
                }
                @Override
                public void onClick(View v) {
                    int vid=v.getId();
                    System.out.println(vid);
                    if(vid==holder.delete.getId()){
                        list.remove(position);
                        id.remove(position);
                        totaldata.remove(position);
                        write();
                        //更新有問題
                        listAdapter.notifyDataSetChanged();
                    }
                }
            }
        }
        //listAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,list);
    public void write(){
        final File mSDFile = new File(Environment.getExternalStorageDirectory().getPath());
        String a = Environment.getExternalStorageDirectory().getAbsolutePath().toString();
        try{
            FileWriter fw = new FileWriter(mSDFile.getAbsolutePath()+"/Data/history.csv");
            BufferedWriter bw = new BufferedWriter(fw);
            //更新csv中的delete值
            bw.write(attr+"\n");
            for(int j=0;j<list.size();j++){
                    bw.write(totaldata.get(j) + "\n");

            }
            bw.close();
            fw.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    int total=0;
    public void update(){
        final File mSDFile = new File(Environment.getExternalStorageDirectory().getPath());
        String a = Environment.getExternalStorageDirectory().getAbsolutePath().toString();
        try{
            FileReader fr = new FileReader(mSDFile.getAbsolutePath()+"/Data/history.csv");
            BufferedReader br = new BufferedReader(fr);
            String temp=" ";
            attr= br.readLine();

            while (temp!=null){
                temp=br.readLine();
                totaldata.add(temp);
                String [] str=temp.split(",");
//                for(int i=0+14*total;i<14*(total+1);i++){
//                    totaldata.add(str[i]);
//                }
                Log.i("info ", str.toString());

                    for(int j = 0; j < 6; j++){
                        data.add(str[j]);
                    //Toast.makeText(getApplicationContext(), "你選擇的是" + data.get(j), Toast.LENGTH_SHORT).show();
                    }
                    id.add(data.get(0));

                    test = data.get(2)+", "+data.get(3)+"\n"+data.get(4)+", Duration: "+data.get(5);
                    list.add(test);
                    for(int j = 5; j >= 0; j--)
                        data.remove(j);


            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults){
        switch (permsRequestCode) {

            case 200:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
               /*get permission success*/

                } else {
               /*get permission fail*/
                }
                break;

            default:
                break;
        }
    }
}

