package project.hscc.runningapp.runtilldie;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;

import project.hscc.runningapp.R;

public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        AppCompatButton btn_eng = (AppCompatButton) findViewById(R.id.btn_eng);
        AppCompatButton btn_ch = (AppCompatButton) findViewById(R.id.btn_ch);

        btn_eng.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // check if user have logged in before
                String email = getSharedPreferences("login_status", MODE_PRIVATE).getString("email", null);
                String passwd = getSharedPreferences("login_status", MODE_PRIVATE).getString("passwd", null);

                if (email == null || passwd == null) {
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getApplicationContext(), RunningActivity.class);
                    startActivity(intent);
                }
            }
        });

        btn_ch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO: Chinese version interface
                // test log out, by clear SharedPreferences
                getSharedPreferences("login_status", MODE_PRIVATE).edit().clear().apply();
            }
        });

    }
}
