package project.hscc.runningapp.runtilldie;

class WekaClassifier {

    public static String classify(Object[] i)
            throws Exception {

        double p = Double.NaN;
        p = WekaClassifier.N7d68a4b80(i);

        String result = "null";
        switch ((int) p){
            case 0:
                result = "Walking";
                break;
            case 1:
                result = "Running";
                break;
            case 2:
                result = "Cycling";
                break;
            case 3:
                result = "Standing";
                break;
        }

        return result;

    }
    static double N7d68a4b80(Object []i) {
        double p = Double.NaN;
        if (i[30] == null) {
            p = 2;
        } else if (((Double) i[30]).doubleValue() <= 5.538924072) {
            p = WekaClassifier.N7ecb17681(i);
        } else if (((Double) i[30]).doubleValue() > 5.538924072) {
            p = WekaClassifier.N5a4aa6436(i);
        }
        return p;
    }
    static double N7ecb17681(Object []i) {
        double p = Double.NaN;
        if (i[13] == null) {
            p = 3;
        } else if (((Double) i[13]).doubleValue() <= 2.0) {
            p = WekaClassifier.N3f6482d42(i);
        } else if (((Double) i[13]).doubleValue() > 2.0) {
            p = WekaClassifier.N67f5d5404(i);
        }
        return p;
    }
    static double N3f6482d42(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 3;
        } else if (((Double) i[1]).doubleValue() <= -0.405621702) {
            p = 3;
        } else if (((Double) i[1]).doubleValue() > -0.405621702) {
            p = WekaClassifier.N5f00e77c3(i);
        }
        return p;
    }
    static double N5f00e77c3(Object []i) {
        double p = Double.NaN;
        if (i[30] == null) {
            p = 2;
        } else if (((Double) i[30]).doubleValue() <= 4.411751423) {
            p = 2;
        } else if (((Double) i[30]).doubleValue() > 4.411751423) {
            p = 1;
        }
        return p;
    }
    static double N67f5d5404(Object []i) {
        double p = Double.NaN;
        if (i[11] == null) {
            p = 2;
        } else if (((Double) i[11]).doubleValue() <= 10.45870318) {
            p = 2;
        } else if (((Double) i[11]).doubleValue() > 10.45870318) {
            p = WekaClassifier.N41a3fe815(i);
        }
        return p;
    }
    static double N41a3fe815(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 0;
        } else if (((Double) i[1]).doubleValue() <= -0.169896938) {
            p = 0;
        } else if (((Double) i[1]).doubleValue() > -0.169896938) {
            p = 2;
        }
        return p;
    }
    static double N5a4aa6436(Object []i) {
        double p = Double.NaN;
        if (i[5] == null) {
            p = 0;
        } else if (((Double) i[5]).doubleValue() <= 10.08709183) {
            p = 0;
        } else if (((Double) i[5]).doubleValue() > 10.08709183) {
            p = 1;
        }
        return p;
    }
}
