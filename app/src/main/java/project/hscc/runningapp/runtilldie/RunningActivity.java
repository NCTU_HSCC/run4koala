package project.hscc.runningapp.runtilldie;

import android.Manifest;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import cc.nctu1210.api.koala6x.KoalaDevice;
import cc.nctu1210.api.koala6x.KoalaService;
import cc.nctu1210.api.koala6x.KoalaServiceManager;
import cc.nctu1210.api.koala6x.SensorEvent;
import cc.nctu1210.api.koala6x.SensorEventListener;
import project.hscc.runningapp.R;
import project.hscc.runningapp.tool.ApplicationContext;
import project.hscc.runningapp.view.ScanListAdapter;

public class RunningActivity extends AppCompatActivity implements SensorEventListener, LocationListener, AdapterView.OnItemClickListener,
        OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = RunningActivity.class.getSimpleName();

    //ToDO:sanket 1) ---------Declare variables---------------------------------------------------//
    int windowSize =98;             //Sengmentation 1sec = 49samples ,,,, 2sec=98 samples
    ArrayList<Double> xAccel= new ArrayList<Double>();
    ArrayList<Double> yAccel= new ArrayList<Double>();
    ArrayList<Double> zAccel= new ArrayList<Double>();
    //--------------------------------------------------------------------------------------------//




    //Koala services
    private String Connected_MacAddr;
    private static KoalaDevice Connected_device;
    private KoalaServiceManager mServiceManager;

    public static ArrayList<KoalaDevice> mDevices = new ArrayList<>();  // Manage the devices
    public static ArrayList<AtomicBoolean> mFlags = new ArrayList<>();
    private ArrayList<ModelObject> mObjects = new ArrayList<ModelObject>();
    private ArrayList<String> mMacs = new ArrayList<>();

    //Koala RawData variables
    private final static double acc_val[] = new double[3];
    private final static double gyr_val[] = new double[3];
    private double accX_Prev = 0;
    private double accY_Prev = 0;
    private double accZ_Prev = 0;
    private double gyroX_Prev = 0;
    private double gyroY_Prev = 0;
    private double gyroZ_Prev = 0;
    private double gyr_avrg = 0;

    //BLE services
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeScanner mBLEScanner;
    private ScanCallback mScanCallback; //For Lollipop+
    private BluetoothAdapter.LeScanCallback mLeScanCallback; //For JellyBean and Kitkat
    private ScanSettings settings;
    private List<ScanFilter> filters;
    private ScanListAdapter mScanListAdapter;
    private String mac = "";

    //GPS and GoogleMap services
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    LocationManager locationManager;
    LocationRequest mLocationRequest;

    //Connection flags
    private boolean isConnected = false; // connected to KoalaDevice
    private boolean isStart = false; // start accepting data transmission from KoalaDevice
    private boolean isRun = false; // user is in moving state
    private boolean start_recording = false;
    private boolean mBooleanServiceCreated = false; // KoalaService is created or not
    private static boolean deviceConnectedStatus = false;

    //UI to update programmatically
    TextView uiMacAddress;
    Chronometer mChronometer;
    TextView uiRunDistance;
    TextView uiStepCounter;
    TextView uiPace;
    TextView uiCalories;
    TextView uiCadence;
    TextView uiLegActivities;

    //UI related
    ListView listScan;
    Button btnConnect, btnStart, btnPause, btnDisconnect;
    Spinner spinner_devselector;

    //UI for user interaction
    private AlertDialog dialog;
    private ProgressDialog progressDialog;

    // ==============================Variable for STEP Activities==============================
    private boolean firstTime = true;
    private long current_Time = 0, start_Time = 0;
    int count = 0;

    private int indexRawDataBuffer = 0;
    private final static double accX[][] = new double[2][200];
    private final static double accY[][] = new double[2][200];
    private final static double accZ[][] = new double[2][200];
    private final static double gyroX[][] = new double[2][200];
    private final static double gyroY[][] = new double[2][200];
    private final static double gyroZ[][] = new double[2][200];
    //============================================================STEP COUNTER
    Date date;
    private int peak_count = 0, step_count = 0;
    private boolean timeflag = false;
    private long initial_time = 0;
    //============================================================STEP FREQUENCY
    private double ftxtCadence = -1; // a stride rate. number of steps a runner takes per minute . ideal=170/min
    private double landTime = 0;
    //============================================================STEP LENGTH
    private double min_acc_z = -1;  // minimum z_ac
    private double max_acc_z = 1;  //maximum z_ac
    private boolean acc_z_min_flag = true, acc_z_max_flag = false;
    private double ftxtSteplength = -1;
    //============================================================STEP LANDING
    private double ftxtPace = -1;
    private double ftxtCounterforce = -1;
    private double flandType = -1;
    private double ftxtGCT = -1;
    private double ftxtVerticalOscillation = -1;
    //============================================================SPEED AND DISTANCE
    Location lastPositionLocation = new Location("point A");

    private double ftxtDistanceInMeter = 0;
    private double ftxtTotalDistanceInMeter = 0;
    private double ftxtSpeed = 0;
    //============================================================ETC
    private long ftxtDuration;
    private double ftxtCalories;
    //==============================END OF Variable for STEP Activities==============================

    //constanta
    private double peak_acc_x = 2.5;

    //Permissions
    public static final int REQUEST_FINE_LOCATION = 0x01 << 11;
    public static final int REQUEST_COARSE_LOCATION = 0x01 << 12;
    public static final int REQUEST_EXTERNAL_STORAGE = 0x01 << 13;

    public static final String[] PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    // ---------------------------------------------------------------------------------------------  Override procedures

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_running);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Toolbar mToolbar =(Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        // DONT FORGET TO REMOVE THIS
        ApplicationContext.bodyMassKg = 60;

        initUI();
        resetUiText();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!checkStoragePermissions(this)) requestStoragePermissions(this);
            if (!checkCoarseLocationPermissions(this)) requestCoarseLocationPermissions(this);
//            if (!checkFineLocationPermissions(this)) requestFineLocationPermissions(this);

        }

        initBLE();
        initKoala();
        initLocationManager();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ModelObject object = mScanListAdapter.getData().get(position);
        mac = object.getAddress();
        spinner_devselector.setSelection(findKoalaWithMac(mac));

        new AlertDialog.Builder(RunningActivity.this)
                .setTitle("Check ?")
                .setMessage("Connect to " + mac + " ?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        progressDialog = ProgressDialog.show(RunningActivity.this, "Connecting", "Please wait ...", true);
                        startRun();
                        DismissDialogListView();

                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

    private void startRun() {
        if (spinner_devselector.getCount() >= 1) {
            mac = spinner_devselector.getSelectedItem().toString();

            Connected_device = mDevices.get(findKoalaWithMac(mac));
            Connected_device.resetSamplingRate();
            Connected_device.setConnectedTime();
            Connected_MacAddr = mac;

            if (mBooleanServiceCreated) {
                if (!isConnected) {
                    mServiceManager.connect(mac);
                    Toast.makeText(getBaseContext(), "Connection established to " + mac, Toast.LENGTH_SHORT).show();
                    //Update MAC
                    ApplicationContext.mac = mac; //=======================================update UI MacAddress
                    uiMacAddress.setText(mac);

                    spinner_devselector.setEnabled(false);
                    isConnected = !isConnected;
                } else {
                    mServiceManager.disconnect();
                    Toast.makeText(getBaseContext(), "Disconnected from " + mac, Toast.LENGTH_SHORT).show();
                    Connected_device = null;
                    Connected_MacAddr = "";
                    uiMacAddress.setText(R.string.EmptyDashString); //=======================================update UI MacAddress

                    spinner_devselector.setEnabled(true);
                    isConnected = !isConnected;
                }

            } else {
                Toast.makeText(getBaseContext(), "Failed to connect to : " + mac, Toast.LENGTH_SHORT).show();
                spinner_devselector.setEnabled(true);
                isConnected = false;
            }
        } else {
            Toast.makeText(getBaseContext(), "Scanner returned nothing", Toast.LENGTH_SHORT).show();
            spinner_devselector.setEnabled(true);
            isConnected = false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mManuinflater = getMenuInflater();
        mManuinflater.inflate(R.menu.mywork,menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent;
        switch (item.getItemId()) {
            case R.id.stat:
                intent = new Intent(this, Main2Activity.class);
                startActivity(intent);
                return true;

            case R.id.hist:
                intent = new Intent(this, HistoryActivity.class);
                startActivity(intent);
                return true;

            case R.id.ref:
                intent = new Intent(this, Main3Activity.class);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        buildGoogleApiClient();

        // setup gmap options
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        try {
            mMap.setMyLocationEnabled(true);
        } catch (SecurityException sExc){
            if (!checkCoarseLocationPermissions(this)) requestCoarseLocationPermissions(this);
            if (!checkFineLocationPermissions(this)) requestFineLocationPermissions(this);
        }
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
    }

    @Override
    public void onConnected(Bundle bundle) {
        createLocationRequest();
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        //Log.d("trig","loc triged");
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));

        if (isStart) {

            //Update running status
//            ApplicationContext.gps_speed = location.calcMovementData();
//            ApplicationContext.gps_latitude = location.getLatitude();
//            ApplicationContext.gps_longitude = location.getLongitude();
//            ApplicationContext.gps_height = location.getAltitude();
//            ApplicationContext.record_time = location.getTime();
            calcMovementData(lastPositionLocation, location);
            lastPositionLocation = location;

            //=======================================update UI distance, pace, calories
            uiRunDistance.setText(String.valueOf(ftxtTotalDistanceInMeter));
            uiPace.setText(String.valueOf(ftxtPace));
            uiCalories.setText(String.valueOf(ftxtCalories));

            //stop location updates
            if (mGoogleApiClient != null) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            }
        }
    }

    @Override
    public void onSensorChange(final SensorEvent e) {
        if (isStart) {
            final int eventType = e.type;
            switch (eventType) {
                case SensorEvent.TYPE_ACCELEROMETER:
                    acc_val[0] = e.values[0];
                    acc_val[1] = e.values[1];
                    acc_val[2] = e.values[2];
                    lowPassFilter();

                    //ToDO:sanket 2)-------"add accelerometer data"-----------------------------------//
                    //1) Add data
                    //2) Calculate features
                    //3) Call classifier
                    //4) print results

                    if (xAccel.size() < windowSize){
                        xAccel.add((double)acc_val[0]);
                        yAccel.add((double)acc_val[1]);
                        zAccel.add((double)acc_val[2]);
                    }else if (xAccel.size() == windowSize){
                        //----------"If window full extracr feature and recognize ativity"------------//

                        FeatureExtract FE = new FeatureExtract();
                        Object[] fetures = FE.main(xAccel,yAccel,zAccel).toArray();

                        try {
                            uiLegActivities.setText(WekaClassifier.classify(fetures));

                            Log.i("Activity result:",WekaClassifier.classify(fetures));
                            xAccel.clear();
                            yAccel.clear();
                            zAccel.clear();
                        } catch (Exception er) {
                            er.printStackTrace();
                        }
                    }
                    //--------------------------------------------------------------------------------//


                    if (isRun) {
                        step_count_detection(acc_val[0], acc_val[2]);
                        uiStepCounter.setText(String.valueOf(step_count)); //=======================================update UI step counter
                        step_distance1(acc_val[2]);
                        landTimeDetection();
                        step_frequency();
                    }
//                        else txtStep.setText(getString(R.string.txt_noAction));

                    // Update running status
                    ApplicationContext.step_count = step_count;
                    ApplicationContext.step_frequency = ftxtCadence;
                    ApplicationContext.step_amplitude = ftxtSteplength;
                    ApplicationContext.pace = ftxtPace;
                    ApplicationContext.landing_impact = ftxtCounterforce;
                    if (flandType == 1) {
                        ApplicationContext.running_type = String.valueOf(R.string.feet_activity_walk);
                    } else if (flandType == 2) {
                        ApplicationContext.running_type = String.valueOf(R.string.feet_activity_run);
                    } else {
                        ApplicationContext.running_type = String.valueOf(R.string.feet_activity_unknown);
                    }
                    //uiLegActivities.setText(ApplicationContext.running_type); //=====================================================================update UI leg activity
                    ApplicationContext.landing_time = ftxtGCT;
                    ApplicationContext.vertical_amplitude = ftxtVerticalOscillation;

                    //else if(PANEL_UPDATE_COUNTER++ % PANEL_UPDATE_INTERVAL == 0)

                    break;
                case SensorEvent.TYPE_GYROSCOPE:
                    gyr_val[0] = e.values[0];
                    gyr_val[1] = e.values[1];
                    gyr_val[2] = e.values[2];

                    gyr_avrg = Math.sqrt(e.values[0] * e.values[0] + e.values[1] * e.values[1] +
                            e.values[2] * e.values[2]) / 3;

                    detectBreakOrRun(gyr_avrg);
                    break;
            }
        }
    }

    @Override
    public void onConnectionStatusChange(boolean status) {
        deviceConnectedStatus = status;
        scanLeDevice(!deviceConnectedStatus);
        if (status){//connected
            progressDialog.dismiss();
            btnConnect.setVisibility(View.GONE);
            btnStart.setVisibility(View.VISIBLE);
            btnDisconnect.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRSSIChange(String addr, float rssi) {

    }

    @Override
    public void onKoalaServiceStatusChanged(boolean status) {
        mBooleanServiceCreated = status;
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // ========================================================================================= BLE
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, 1);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                /* Lollipop +  */
                mBLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
                settings = new ScanSettings.Builder()
                        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                        .build();
                filters = new ArrayList<ScanFilter>();

                mScanCallback = new ScanCallback() {
                    @Override
                    public void onScanResult(int callbackType, ScanResult result) {
                        final ScanResult scanResult = result;
                        final BluetoothDevice device = scanResult.getDevice();
                        new Thread() {
                            @Override
                            public void run() {
                                if (device != null) {
                                    if (!start_recording) {
                                        KoalaDevice p = new KoalaDevice(device, scanResult.getRssi(), scanResult.getScanRecord().getBytes());
                                        int position = findKoalaWithMac(device.getAddress());
                                        if (position == -1) {
                                            AtomicBoolean flag = new AtomicBoolean(false);
                                            mDevices.add(p);
                                            mFlags.add(flag);
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    // This code will always run on the UI thread, therefore is safe to modify UI elements.
                                                    loadSpinner();
                                                    mScanListAdapter.notifyDataSetChanged();
                                                }
                                            });
                                        }
                                    } else {
                                        //automatic connection
                                        if (!deviceConnectedStatus) {
                                            String address = device.getAddress();
                                            if (address.equals(ApplicationContext.mac)) {
                                                mServiceManager.connect(address);
                                            }
                                        }
                                    }
                                }
                            }
                        }.start();
                    }
                };
            } else {
                /* JellyBean/Kitkat  */
                mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
                    @Override
                    public void onLeScan(final BluetoothDevice device, final int rssi,
                                         final byte[] scanRecord) {
                        new Thread() {
                            @Override
                            public void run() {
                                if (device != null) {
                                    if (!start_recording) {
                                        KoalaDevice p = new KoalaDevice(device, rssi, scanRecord);
                                        int position = findKoalaWithMac(device.getAddress());
                                        if (position == -1) {
                                            AtomicBoolean flag = new AtomicBoolean(false);
                                            mDevices.add(p);
                                            mFlags.add(flag);
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    // This code will always run on the UI thread, therefore is safe to modify UI elements.
                                                    loadSpinner();
                                                    mScanListAdapter.notifyDataSetChanged();
                                                }
                                            });
                                        }
                                    } else {
                                        //automatic connection
                                        if (!deviceConnectedStatus) {
                                            String address = device.getAddress();
                                            if (address.equals(ApplicationContext.mac)) {
                                                mServiceManager.connect(address);
                                            }
                                        }
                                    }
                                }
                            }
                        }.start();
                    }
                };
            }
        }

        // ========================================================================================= GPS location
        if (mGoogleApiClient != null){
            if (mGoogleApiClient.isConnected()){
                if (!checkCoarseLocationPermissions(this)) requestCoarseLocationPermissions(this);
                if (!checkFineLocationPermissions(this)) requestFineLocationPermissions(this);
                startLocationUpdates();
            }
        }
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();

        for (int i = 0; i < mFlags.size(); i++) {
            AtomicBoolean flag = mFlags.get(i);
            flag.set(false);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mServiceManager != null) {
            mServiceManager.unRegisterSensorEventListener(this, SensorEvent.TYPE_ACCELEROMETER);
            mServiceManager.unRegisterSensorEventListener(this, SensorEvent.TYPE_GYROSCOPE);
//            mServiceManager.unRegisterSensorEventListener(this, SensorEvent.TYPE_MAGNETOMETER);
            mServiceManager.close();
        }

        System.exit(0);
    }

    // --------------------------------------------------------------------------------------------- Permissions

    public boolean checkStoragePermissions(RunningActivity activity) {
        // Check if we have write permission
        return ActivityCompat.checkSelfPermission(activity, PERMISSIONS[1]) == PackageManager.PERMISSION_GRANTED;
    }
    public static void requestStoragePermissions(RunningActivity activity) {
        ActivityCompat.requestPermissions(activity,new String[]{PERMISSIONS[1]},REQUEST_EXTERNAL_STORAGE);
    }

    public boolean checkCoarseLocationPermissions(RunningActivity activity) {
        // Check if we can access location in coarse mode
        return ActivityCompat.checkSelfPermission(activity, PERMISSIONS[2]) == PackageManager.PERMISSION_GRANTED;
    }
    public static void requestCoarseLocationPermissions(RunningActivity activity) {
        ActivityCompat.requestPermissions(activity,new String[]{PERMISSIONS[2]}, REQUEST_COARSE_LOCATION);
    }

    public boolean checkFineLocationPermissions(RunningActivity activity) {
        // Check if we can access location in fine mode
        return ActivityCompat.checkSelfPermission(activity, PERMISSIONS[3]) == PackageManager.PERMISSION_GRANTED;
    }
    public static void requestFineLocationPermissions(RunningActivity activity) {
        ActivityCompat.requestPermissions(activity, new String[]{PERMISSIONS[3]}, REQUEST_FINE_LOCATION);
    }

    // --------------------------------------------------------------------------------------------- Location / GPS

    protected synchronized void buildGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(3000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void startLocationUpdates() {
        try {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } catch (SecurityException sExc){
            if (!checkCoarseLocationPermissions(this)) requestCoarseLocationPermissions(this);
            if (!checkFineLocationPermissions(this)) requestFineLocationPermissions(this);
        }
    }

    protected void stopLocationUpdates() {
//        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    private void initLocationManager() {
        lastPositionLocation.setLatitude(0.0d);
        lastPositionLocation.setLatitude(0.0d);
        lastPositionLocation.setTime(System.currentTimeMillis());

        createLocationRequest();
    }

    // --------------------------------------------------------------------------------------------- UI and Tools Initialization

    public void initUI() {
        // --- texts
        uiMacAddress = (TextView) findViewById(R.id.ConnectedKoalaMacAddress);
        uiRunDistance = (TextView) findViewById(R.id.running_distance);
        uiStepCounter = (TextView) findViewById(R.id.running_step_counter);
        uiPace = (TextView) findViewById(R.id.running_pace);
        uiCalories = (TextView) findViewById(R.id.running_calories);
        uiCadence = (TextView) findViewById(R.id.running_cadence);
        uiLegActivities = (TextView) findViewById(R.id.feet_activity);
        mChronometer = (Chronometer) findViewById(R.id.running_duration);
        mChronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                // update duration when user is running
                ftxtDuration = (SystemClock.elapsedRealtime() - chronometer.getBase()) / 1000;
            }
        });

        // --- buttons
        btnConnect = (Button) findViewById(R.id.btn_connect);
        btnConnect.setOnClickListener(mConnectToKoala);
        btnStart = (Button) findViewById(R.id.btn_startRun);
        btnStart.setOnClickListener(mStartListener);
        btnPause = (Button) findViewById(R.id.btn_stopRun);
        btnPause.setOnClickListener(mPauseListener);
        btnDisconnect = (Button) findViewById(R.id.btn_disconnect);
        btnDisconnect.setOnClickListener(mDisconnectFromKoala);
        spinner_devselector = (Spinner) findViewById(R.id.spinner2);
        spinner_devselector.bringToFront();
    }

    private void resetUiText(){
        mChronometer.setText(R.string.zeroClock);
        uiRunDistance.setText(R.string.zeroNumber);
        uiStepCounter.setText(R.string.zeroNumber);
        uiPace.setText(R.string.zeroNumber);
        uiCalories.setText(R.string.zeroNumber);
        uiCadence.setText(R.string.zeroNumber);
        uiLegActivities.setText(R.string.feet_activity_unknown);
        uiMacAddress.setText(R.string.EmptyDashString);
    }

    private void initBLE() {
        /* check whether the BLE's device or not */
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "BLE not supported", Toast.LENGTH_SHORT).show();
            finish();
        }

        final BluetoothManager mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "BLE not supported", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
    }

    private void initKoala() {
        mServiceManager = new KoalaServiceManager(RunningActivity.this);
        mServiceManager.registerSensorEventListener(this, SensorEvent.TYPE_ACCELEROMETER, KoalaService.MOTION_WRITE_RATE_20, KoalaService.MOTION_ACCEL_SCALE_8G, KoalaService.MOTION_GYRO_SCALE_1000);
        mServiceManager.registerSensorEventListener(this, SensorEvent.TYPE_GYROSCOPE);
        mServiceManager.registerSensorEventListener(this, SensorEvent.TYPE_MAGNETOMETER);
    }

    // --------------------------------------------------------------------------------------------- Buttons Listener

    View.OnClickListener mConnectToKoala = new View.OnClickListener() {
        public void onClick(View v) {
            showDialogListView();
            scanLeDevice(true);
        }
    };

    View.OnClickListener mStartListener = new View.OnClickListener() {
        public void onClick(View v) {
            mChronometer.setBase(SystemClock.elapsedRealtime() - timeWhenStopped);
            mChronometer.start(); //=======================================update UI duration
            isStart=true;

            btnConnect.setVisibility(View.GONE);
            btnStart.setVisibility(View.GONE);
            btnPause.setVisibility(View.VISIBLE);
            btnDisconnect.setVisibility(View.GONE);
        }
    };

    long timeWhenStopped = 0;
    View.OnClickListener mPauseListener = new View.OnClickListener() {
        public void onClick(View v) {
            mChronometer.stop();
            timeWhenStopped = SystemClock.elapsedRealtime() - mChronometer.getBase();
            isStart=false;

            btnConnect.setVisibility(View.GONE);
            btnStart.setVisibility(View.VISIBLE);
            btnPause.setVisibility(View.GONE);
            btnDisconnect.setVisibility(View.VISIBLE);
        }
    };

    View.OnClickListener mDisconnectFromKoala = new View.OnClickListener() {
        public void onClick(View v) {

            if (mBooleanServiceCreated && isConnected) {
                mServiceManager.disconnect();
                Toast.makeText(getBaseContext(), "Disconnected from " + mac, Toast.LENGTH_SHORT).show();
                Connected_device = null;
                Connected_MacAddr = "";
                uiMacAddress.setText(R.string.EmptyDashString); //=======================================update UI MacAddress
                spinner_devselector.setEnabled(true);
                isConnected = !isConnected;

                //restart chronometer
                timeWhenStopped = 0;
                mChronometer.setBase(SystemClock.elapsedRealtime());

                btnConnect.setVisibility(View.VISIBLE);
                btnStart.setVisibility(View.GONE);
                btnPause.setVisibility(View.GONE);
                btnDisconnect.setVisibility(View.GONE);
                resetUiText();
            }
        }
    };

    // --------------------------------------------------------------------------------------------- Custom procedures

    private void scanLeDevice(boolean scanFlag) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            if (scanFlag)
                mBluetoothAdapter.startLeScan(mLeScanCallback);
            else
                mBluetoothAdapter.stopLeScan(mLeScanCallback);
        } else {
            if (scanFlag) {
                mBLEScanner.startScan(mScanCallback);
            } else
                mBLEScanner.stopScan(mScanCallback);
        }
    }

    private int findKoalaWithMac(String macAddr) {
        if (mDevices.size() == 0)
            return -1;
        for (int i = 0; i < mDevices.size(); i++) {
            KoalaDevice tmpDevice = mDevices.get(i);
            if (macAddr.matches(tmpDevice.getDevice().getAddress()))
                return i;
        }
        return -1;
    }

    private void loadSpinner() {
        mObjects.clear();
        mMacs.clear();
        for (int i = 0, size = mDevices.size(); i < size; i++) {
            KoalaDevice d = mDevices.get(i);
            ModelObject object = new ModelObject(d.getDevice().getName(), d.getDevice().getAddress(), String.valueOf(d.getRssi()));
            mObjects.add(object);
            mMacs.add(object.getAddress());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, mMacs);
        spinner_devselector.setAdapter(adapter);
        if(mac != "")
            spinner_devselector.setSelection(findKoalaWithMac(mac));
    }

    private void showDialogListView(){
        listScan = new ListView(this);
        mScanListAdapter = new ScanListAdapter(this,mObjects);
        listScan.setAdapter(mScanListAdapter);
        listScan.setOnItemClickListener(this);
        AlertDialog.Builder builder= new AlertDialog.Builder(this);
        builder.setTitle("Scanning");
        builder.setCancelable(true);
        builder.setPositiveButton("Cancel",null);
        builder.setView(listScan);
        dialog = builder.create();
        dialog.show();
    }

    private void DismissDialogListView(){
        start_recording = !start_recording;
        scanLeDevice(false);
        dialog.dismiss();
    }

    // --------------------------------------------------------------------------------------------- Koala Data Processing
    private void lowPassFilter() {
        acc_val[0] = accX_Prev * 0.3 + acc_val[0] * 0.7;
        acc_val[1] = accY_Prev * 0.3 + acc_val[1] * 0.7;
        acc_val[2] = accZ_Prev * 0.3 + acc_val[2] * 0.7;

        gyr_val[0] = gyroX_Prev * 0.3 + gyr_val[0] * 0.7;
        gyr_val[1] = gyroY_Prev * 0.3 + gyr_val[1] * 0.7;
        gyr_val[2] = gyroZ_Prev * 0.3 + gyr_val[2] * 0.7;

        accX_Prev = acc_val[0];
        accY_Prev = acc_val[1];
        accZ_Prev = acc_val[2];

        gyroX_Prev = gyr_val[0];
        gyroY_Prev = gyr_val[1];
        gyroZ_Prev = gyr_val[2];

    }

    private void loadRawData() {
        if (indexRawDataBuffer >= 0 && indexRawDataBuffer < 100) {

            accX[0][indexRawDataBuffer] = acc_val[0];
            accY[0][indexRawDataBuffer] = acc_val[1];
            accZ[0][indexRawDataBuffer] = acc_val[2];
            gyroX[0][indexRawDataBuffer] = gyr_val[0];
            gyroY[0][indexRawDataBuffer] = gyr_val[1];
            gyroZ[0][indexRawDataBuffer] = gyr_val[2];

        } else if (indexRawDataBuffer >= 100 && indexRawDataBuffer < 200) {

            accX[0][indexRawDataBuffer] = acc_val[0];
            accY[0][indexRawDataBuffer] = acc_val[1];
            accZ[0][indexRawDataBuffer] = acc_val[2];
            gyroX[0][indexRawDataBuffer] = gyr_val[0];
            gyroY[0][indexRawDataBuffer] = gyr_val[1];
            gyroZ[0][indexRawDataBuffer] = gyr_val[2];

            accX[1][indexRawDataBuffer - 100] = acc_val[0];
            accY[1][indexRawDataBuffer - 100] = acc_val[1];
            accZ[1][indexRawDataBuffer - 100] = acc_val[2];
            gyroX[1][indexRawDataBuffer - 100] = gyr_val[0];
            gyroY[1][indexRawDataBuffer - 100] = gyr_val[1];
            gyroZ[1][indexRawDataBuffer - 100] = gyr_val[2];

        } else if (indexRawDataBuffer >= 200 && indexRawDataBuffer < 300) {

            accX[1][indexRawDataBuffer - 100] = acc_val[0];
            accY[1][indexRawDataBuffer - 100] = acc_val[1];
            accZ[1][indexRawDataBuffer - 100] = acc_val[2];
            gyroX[1][indexRawDataBuffer - 100] = gyr_val[0];
            gyroY[1][indexRawDataBuffer - 100] = gyr_val[1];
            gyroZ[1][indexRawDataBuffer - 100] = gyr_val[2];

        }

        indexRawDataBuffer++;
        if (indexRawDataBuffer == 300) {
            indexRawDataBuffer = 0;
        }

    }

    // --------------------------------------------------------------------------------------------- Activity Processing
    private void detectBreakOrRun(double gyro) {
        current_Time = new Date().getTime();

        if(firstTime){
            start_Time = current_Time;
            firstTime = false;
        }
        if(gyro > 77) {
            count++;
        }

        if((current_Time - start_Time) > 2000){

            isRun = count >= 7;
            count = 0;
            firstTime = true;
        }
    }


    private double peakAccX_value = 0;
    private double peakAccX_time = 0;
    private boolean peakAccX_Flag = false;
    private final static double peakAccZ_value[] = new double[3];
    private double peakAccZ_value_temp = 0;
    private int peakAccZ_index = 0;
    private double avrAccZ = 0;
    private boolean verticalDisplacementFlag = false;
    private double verticalDisplacement = 0;  // display this value  it's the displacement of leg.
    private double verticalDisplacement_LastTime = 0;
    private final static double recordStrke_gyroY[] = new double[100];
    private int recordIndexGyroY = 0;
    private boolean strikeTypeFlag = false;
    private double recordStrke_gyroY_2 = 0;
    private double peakGyroY_value = 0;
    private double peakGyroY_time = 0;

    private void landTimeDetection() {
        loadRawData();
        double currentTime = java.lang.System.nanoTime() / 1e9;

        //if(landTimeFunctionFlag) {
        if (acc_val[0] >= peakAccX_value) {
            // ================================== determine vertical displacement ==================================
            if (verticalDisplacementFlag == true) {
                double time = (currentTime - verticalDisplacement_LastTime) - landTime;
                if (time > 0 && time < 0.5) {
                    verticalDisplacement = 0.5 * 0.98 * time * time;
                    ftxtVerticalOscillation = verticalDisplacement * 100;
                }
                verticalDisplacement_LastTime = currentTime;

            } else {
                verticalDisplacementFlag = true;
                verticalDisplacement_LastTime = currentTime;
            }

            peakAccX_value = acc_val[0];
            //peakAccX_index = indexRawDataBuffer;
            peakAccX_time = currentTime;

            // strike type event
            recordStrke_gyroY_2 = gyr_val[1];
            strikeTypeFlag = true;

            recordIndexGyroY = 0;
            for (int i = 0; i < 100; i++)
                recordStrke_gyroY[i] = 0;
            recordStrke_gyroY[recordIndexGyroY] = gyr_val[1];
            recordIndexGyroY++;
            strikeTypeFlag = true;
            //=====================================================================================================

            peakAccZ_value_temp = acc_val[2];
            peakAccX_Flag = true;
        }

        if (strikeTypeFlag == true && currentTime <= (peakAccX_time + 0.2)) {
            recordStrke_gyroY[recordIndexGyroY] = gyr_val[1];
            recordIndexGyroY++;

        } else if (strikeTypeFlag == true && currentTime > (peakAccX_time + 0.2)) {
            double sum = 0;

            for (int i = 0; i < (recordIndexGyroY - 1); i++) {
                sum = recordStrke_gyroY[i + 1] - recordStrke_gyroY[i] + sum;
            }

            sum = sum / (recordIndexGyroY - 1);

            strikeTypeFlag = false;
        }

        if (peakAccX_Flag == true && gyr_val[1] >= peakGyroY_value && currentTime > (peakAccX_time + 0.2) && currentTime < (peakAccX_time + 1)) {
            peakGyroY_value = gyr_val[1];
            peakGyroY_time = currentTime;
        }

        if (peakAccX_Flag == true && currentTime > (peakAccX_time + 1)) {
            landTime = peakGyroY_time - peakAccX_time;
            if (landTime > 0) {
                ftxtGCT = landTime;
            }

            //strike========================== test
            if (recordStrke_gyroY_2 <= -0) {
                flandType = 1; //heel
            } else if (recordStrke_gyroY_2 > 0) {
                flandType = 2; //toe
            }

            peakAccZ_value[peakAccZ_index] = peakAccZ_value_temp;
            peakAccZ_index++;
            if (peakAccZ_index == 3) {
                peakAccZ_index = 0;
            }

            if (peakAccZ_value[0] != 0 && peakAccZ_value[1] != 0 && peakAccZ_value[2] != 0) {
                avrAccZ = (peakAccZ_value[0] + peakAccZ_value[1] + peakAccZ_value[2]) / 3;
                avrAccZ = Math.abs(avrAccZ);
                ftxtCounterforce = avrAccZ / 9.8;
            }


            peakAccX_value = 2.5;
            peakAccX_time = currentTime;

            peakGyroY_value = 0;
            peakGyroY_time = currentTime;

            peakAccX_Flag = false;
        }
    }

    public void step_count_detection(double acc_x, double acc_z) {
        date = new Date();
        long time = date.getTime();
        // detect peak happens
        if (acc_x >= peak_acc_x) {
            initial_time = date.getTime();
            timeflag = true;
            peak_acc_x = acc_x;
        }

        else if ((time - initial_time) >= 200 && acc_x <= -0.5 && timeflag) {
            timeflag = false;
            peak_count++;
            step_count = peak_count * 2 - 1;
            peak_acc_x = 2.5;
        }
    }

    public void step_frequency() {
        if(isRun) {
            if (landTime > 0) {
                ftxtCadence = (60 / landTime) * 2;
                uiCadence.setText(String.valueOf(ftxtCadence)); //=======================================update UI step frequency
                ApplicationContext.step_frequency = ftxtCadence;
            }
//            else txtCadence.setText("0");
        }
//        else{
//            txtCadence.setText("0");
//        }
    }

    public void step_distance1(double acc_z) {
        if (acc_z <= min_acc_z && acc_z_min_flag) {
            min_acc_z = acc_z;
            acc_z_max_flag = true;
        }
        if (acc_z >= max_acc_z && acc_z_max_flag) {
            max_acc_z = acc_z;
            acc_z_min_flag = false;
        }
        if (!acc_z_min_flag && acc_z_max_flag && acc_z <= 0) {
            ftxtSteplength = Math.pow((max_acc_z - min_acc_z), 0.25) * 0.5 * 100;
            ApplicationContext.step_amplitude = ftxtSteplength;
//            txtStepAmplitude.setText(String.valueOf((int) ftxtSteplength));
            acc_z_min_flag = true;
            acc_z_max_flag = false;
            min_acc_z = -1;
            max_acc_z = 1;
        }
    }

    public void calcMovementData(Location locationA, Location locationB) {
        // calculate distance in meters
        ftxtDistanceInMeter = locationA.distanceTo(locationB);
        ftxtTotalDistanceInMeter += ftxtDistanceInMeter;

        // calculate speed in m/s
        if (locationB.hasSpeed()){
            ftxtSpeed = locationA.getSpeed();
            if (ftxtSpeed>=1000){ // impossible number, switch to manual calculation
                ftxtSpeed = Math.sqrt(
                        Math.pow(locationB.getLongitude() - locationA.getLongitude(), 2) + Math.pow(locationB.getLatitude() - locationA.getLatitude(), 2)
                ) / (locationB.getTime() - locationA.getTime());
            }
        }else // manual calculation
            ftxtSpeed = Math.sqrt(
                    Math.pow(locationB.getLongitude() - locationA.getLongitude(), 2) + Math.pow(locationB.getLatitude() - locationA.getLatitude(), 2)
            ) / (locationB.getTime() - locationA.getTime());
        // int speed=(int) ((location.getSpeed()*3600)/1000); --> for kmh
        // int speed=(int) (location.getSpeed()*2.2369); --> for mph

        // calculate pace rate in m/s
        ftxtPace = ftxtTotalDistanceInMeter / ftxtDuration;

        // calculate KCalories ~= METS * bodyMassKg * timePerformingHours
        ftxtCalories += calcMETS(ftxtSpeed) * ApplicationContext.bodyMassKg * (ftxtDuration/3600);


        /* source
        distance
        http://stackoverflow.com/questions/8049612/calculating-distance-between-two-geographic-locations

        speed
        http://stackoverflow.com/questions/4811920/why-getspeed-always-return-0-on-android
        http://stackoverflow.com/questions/15570542/determining-the-speed-of-a-vehicle-using-gps-in-android
        http://www.programcreek.com/java-api-examples/index.php?class=android.location.Location&method=getSpeed | check: Example 11

        pace
        https://www.quora.com/What-is-the-formula-for-pace-calculation-in-running

        calories
        http://fitness.stackexchange.com/questions/15608/energy-expenditure-calories-burned-equation-for-running
         */
    }

    public double calcMETS(double speedInMps){
        double speedInKmh = speedInMps * 3.6;

        if (speedInKmh<=3.475904){
            return 6;
        } else if(speedInKmh<=4.34488){
            return 8.3;
        } else if(speedInKmh<=4.5186752){
            return 9;
        } else if(speedInKmh<=5.213856){
            return 9.8;
        } else if(speedInKmh<=5.8221392){
            return 10.5;
        } else if(speedInKmh<=6.082832){
            return 11;
        } else if(speedInKmh<=6.51732){
            return 11.5;
        } else if(speedInKmh<=6.951808){
            return 11.8;
        } else if(speedInKmh<=7.4731936){
            return 12.3;
        } else if(speedInKmh<=7.820784){
            return 12.8;
        } else if(speedInKmh<=8.68976){
            return 14.5;
        } else if(speedInKmh<=9.558736){
            return 16;
        } else if(speedInKmh<=10.427712){
            return 19;
        } else if(speedInKmh<=11.296688){
            return 19.8;
        } else {
            return 23;
        }

        /*
        METS calculation according to http://golf.procon.org/view.resource.php?resourceID=004786
        convert original input from mph to kph
         */
    }
}
